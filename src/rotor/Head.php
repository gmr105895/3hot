<?php

namespace rotor;

#[\Attribute]
class Head extends Route {
    public function __construct($route) {
        parent::__construct($route, ['HEAD']);
    }
}
