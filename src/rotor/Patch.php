<?php

namespace rotor;

#[\Attribute]
class Patch extends Route {
    public function __construct($route) {
        parent::__construct($route, ['PATCH']);
    }
}
