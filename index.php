<?php

require 'src/autoload.php';

use rotor\Exact;
use rotor\Get;
use rotor\Fallback;
use rotor\Schema;

use viewer\View;

class App {
    #[Exact]
    #[Get('/')]
    function index() {
        try {
            echo View::fromFile('views/Layout.php', [
                'body' => View::fromFile('views/Welcome.php', [
                    'username' => 'World',
                ])
            ]);
        } catch (ValueError $e) {
            $this->fallback();
        }
    }

    #[Get('/welcome/?(.*)')]
    function welcome($params) {
        [$username] = $params;

        try {
            echo View::fromFile('views/Welcome.php', [
                'username' => $username ?: 'Guest'
            ]);
        } catch (ValueError $e) {
            $this->fallback();
        }
    }

    #[Get('/fail')]
    function fail() {
        try {
            echo View::fromFile('views/NotExistingFile.php');
        } catch(ValueError $e) {
            $this->fallback();
        }
    }

    #[Fallback]
    function fallback() {
        http_response_code(404);
        echo 'Not Found';
    }
}

function basePath($uri, $paramsSeparator = '?') {
    $end = strrpos($uri, $paramsSeparator);
    if ($end === false)
        return $uri;

    return substr($uri, 0, $end);
}

Schema::route(new App(), basePath($_SERVER['REQUEST_URI']), $_SERVER['REQUEST_METHOD']);
